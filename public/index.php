<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blog from scratch</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
<main>
<header>
        <?php
            //pour lier le header :
            include __DIR__.'/../includes/header.php';
        ?>
    </header>
<?php 
        $bdd = new PDO('mysql:host=localhost;dbname=blogfromscratch', 'mysql', 'mysql');
        //faire une requete :
        $reponse = $bdd->query('SELECT * FROM articles');

        //récuperer chaque ligne :
        while ($donnees = $reponse->fetch()){
            $content = strip_tags($donnees['content']);
            echo '<div class= "billet" id='.$donnees['id'].'>
            <h2>'.$donnees['title'].'</h2>
            <img class = "preview" style = "width:200px" src="'.$donnees['image_url'].'" alt="icone">
            <p>'.substr($content,0,300).'</p><p>Date :'.$donnees['published_at'].'</p>
            <a href="/article.php?id='.$donnees['id'].'">Lire la suite</a>
        </div>';

        }

        ?>
    </main>
    <footer>
        <?php
            //pour lier le footer :
            include __DIR__.'/../includes/footer.php';
        ?>
    </footer>
</body>
</html>